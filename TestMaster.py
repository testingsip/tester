from xml.dom.minidom import parse
from PCO import PCO
from UA import UA
from Interaction import Interaction, Annotation
import xml.dom.minidom

class TestMaster:
    pcos = {}
    testDescription = []
    objective = ""
    elementDictionary = {}
    # def __init__(self):
        # TODO: do init

    # parsea un archivo.tdl que recibe como parametro
    def parseXML(self, xmlFile):
        # clean data
        self.testDescription = []
        self.pcos = {}
        self.elementDictionary = {}
        self.objective = ""

        # start parse
        DOMTree =  xml.dom.minidom.parse(xmlFile)
        package =  DOMTree.documentElement
        packageElements = package.getElementsByTagName("packagedElements")
        # recorremos todos los elementos del archivo tdl
        index = 0;
        for node in packageElements:
            nodeType = node.getAttribute("xsi:type")
            if(nodeType == "tdl:VerdictType"):
                self.elementDictionary["//@packagedElements." +  str(index)] = node.getAttribute("name")
            elif(nodeType == "tdl:AnnotationType"):
                self.elementDictionary["//@packagedElements." +  str(index)] = node.getAttribute("name")
            elif(nodeType == "tdl:TestObjective"):
                self.objective = node.getAttribute("description")
            elif(nodeType == "tdl:DataSet"):
                self.readDataSet(index, node)
                print nodeType
            elif(nodeType == "tdl:ComponentType"):
                self.elementDictionary["//@packagedElements." +  str(index)] = node.getAttribute("name")
            elif(nodeType == "tdl:GateType"):
                print nodeType
            elif(nodeType == "tdl:TestConfiguration"):
                self.readTestConfiguration(index, node)
            elif(nodeType == "tdl:TestDescription"):
                self.readTestDescription(node)
            index += 1

    # Lee y crea los PCO's y UA's definidos en el archivo tdl
    def readTestConfiguration(self, parentIndex, node):
        print "- ---- Reading Configuration ------ "
        componentInstances = node.getElementsByTagName("componentInstance")
        pco = None
        flag = 0
        # TODO: revisar donde se asignaran las ip de cada pco/ua
        #       y los usuarios segun la ip
        index = 0
        for ci in componentInstances:
            if(flag == 0):
                pco_name = ci.getAttribute("name")
                pco_ip = "xxx.xxx.xxx.xxx" #TODO: hay que cambiarlo
                username = "ua" #TODO: hay que cambiarlo
                pco = PCO(pco_name, pco_ip, username)
                self.elementDictionary["//@packagedElements." + str(parentIndex) + "/@componentInstance." + str(index) + "/@gateInstance.0"] = pco
                flag = 1
            else:
                pco.getUA().setName(ci.getAttribute("name"))
                self.elementDictionary["//@packagedElements." + str(parentIndex) + "/@componentInstance." + str(index) + "/@gateInstance.0"] = pco.getUA()
                self.pcos[pco.getUA().getName()] = pco
                flag = 0
            index += 1
        print "------ finish read of Configuration -------"

    # lee todas las interacciones del archivo tdl
    # TODO: falta las interacciones que representan el resultado
    def readTestDescription(self, description):
        print " ------ Reading test description ------ "
        mainBehaviour = description.getElementsByTagName("behaviour")[0]
        block = mainBehaviour.getElementsByTagName("block")[0]
        interactions = block.getElementsByTagName("behaviour")
        for interaction in interactions:
            element_type = interaction.getAttribute("xsi:type")
            if(element_type == "tdl:Interaction"):
                source = interaction.getAttribute("source")
                source = self.elementDictionary[source]
                target = interaction.getAttribute("target")
                target = self.elementDictionary[target]
                argument = interaction.getElementsByTagName("argument")
                if(argument.length > 0):
                    dataInstance = argument[0].getAttribute("dataInstance")
                    dataInstance = self.elementDictionary[dataInstance]

                annotation_key = interaction.getElementsByTagName("annotation")[0].getAttribute("key")
                annotation_value = interaction.getElementsByTagName("annotation")[0].getAttribute("value")
                annotation_key = self.elementDictionary[annotation_key]

                annotation = Annotation(annotation_key, annotation_value)
                interact = Interaction(source, target, annotation, dataInstance)

                # agregamos a la lista de interacciones
                self.testDescription.append(interact)
        print "Leidas " + str(len(self.testDescription)) + " interacciones"
        print " ------ Finish read test description ------ "

    def readDataSet(self, parentIndex, data):
        dataInstances = data.getElementsByTagName("dataInstance")
        index = 0
        for d in dataInstances:
            self.elementDictionary["//@packagedElements." + str(parentIndex) + "/@dataInstance." + str(index)] = d.getAttribute("name")
            index += 1

    # ejecutar secuencia de test que se encuentra en testDescription
    def executeTest(self):
        for i in self.testDescription:
            # i.execute()
            print i


    def getPCOS(self):
        return self.pcos
