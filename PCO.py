from UA import UA
# This class represent a Point of Control
class PCO:
    userAgent = None
    name = ""

    def __init__(self, name, ip_address, username):
        self.userAgent = UA(name, ip_address, username)
        self.name = name

    # Retorna el UA que esta controlando el PCO
    def getUA(self):
        return self.userAgent

    # Retorna el nombre del PCO
    def getName(self):
        return self.name

    # Envia el codigo de llamada al UA
    def makeCall(self, id):
		self.userAgent.send("m")
		self.userAgent.send(id)

    # 180 SIP code
    def answerRingin(self):
    	self.userAgent.send("a")
    	self.userAgent.send("180")

    # Responde a una llamada con el codigo 100 (Trying)
    # 100 SIP code
	def answerTrying(self):
		self.userAgent.send("a")
		self.userAgent.send("100")

    # Responde a una llamada con el codigo 200
    # 200 SIP code
	def answerOK(self):
		self.userAgent.send("a")
		self.userAgent.send("200")

    # Manda un comando directamente al UA
	def send(self, cmd):
		self.userAgent.send(cmd)

    #TODO: terminar de implementar mas metodos para interactuar con
    #       el UA

    def __str__(self):
        return self.name
