# This class represent a UA # and it will connect by ssh
# extiende de Thread por que se mantiene obteniendo el output del
# shell ssh que tiene la instancia de pjsua
import subprocess
import sys
import threading

class UA(threading.Thread):
    shell = None
    name = ""
    username = ""
    ip_address = ""
    output = ""

    def __init__(self, name, ip, user):
        threading.Thread.__init__(self)
        self.name = name
        self.ip_address = ip
        self.username = user
        # self.shell = spur.SshShell( hostname=ip_address, username=u, password=p, missing_host_key=spur.ssh.MissingHostKey.accept)
        cmdSsh = "ssh -T " + user + "@" + ip
        ## esto requiere que se tenga configurado ssh keys en las maquinas para
        ## que no requiera el password al conectarse
        G_INUNIX=True
        # self.shell = subprocess.Popen(cmdSsh, shell=G_INUNIX, bufsize=0, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=False)

    # Establece un nombre para el UA
    def setName(self, name):
        self.name = name

    # Devuelve el nombre del UA
    def getName(self):
        return self.name

    # Devuelve la ip del UA
    def getIp(self):
        return self.ip

    ## se mantiene obteniendo el output de la terminal
    def run(self):
		G_INUNIX=True
		# self.shell = subprocess.Popen(self.fullcmd, shell=G_INUNIX, bufsize=0, stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=False)
		self.echo = True
		#print "sleep para conectar"
		#self.send(u)
		# print "salio"
		while self.shell.poll() == None:
			line = self.shell.stdout.readline()
			if line == "":
				break;

			#Print the line if echo is ON
			if self.echo:
				print self.name + ": " + line.rstrip()

			#self.lock.acquire()
			self.output += line
			#self.lock.release()
		self.running = False

    # envia comando para la terminal del UA
    def sendCmd(self, cmd):
        print "UA " + self.name + ": " +  cmd
        cmd = cmd + "\n" # si no se le agrega el \n no ejecuta el comando
        result = self.shell.stdin.write(cmd)
        return result

    def __str__(self):
        return self.name + "(ip:" + self.ip_address + ")"
