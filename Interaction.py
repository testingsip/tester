# This represent an Interaction betwen two UA's
#   ie: Invate, ACK
class Interaction:
    source = None # objeto PCO o UA
    target = None #  objeto PCO o UA
    annotation = None
    dataInstance = "" # define el tipo de interaccion

    def __init__(self, source, target, annotation, dataInstance):
        self.source = source
        self.target = target
        self.annotation = annotation
        self.dataInstance = dataInstance

    # Metodo para ejecutar la interaccion dependiendo el tipo
    def execute(self):
        #TODO: implementar este metodo
        return 0

    def __str__(self):
        return str(self.source) + " ---> " + str(self.target) + "\n\t" + str(self.annotation) + " " + str(self.dataInstance)

class Annotation:
    key = ""
    value = ""

    def __init__(self, key, value):
        self.key = key
        self.value = value

    def getKey(self):
        return self.key

    def getValue(self):
        return self.value

    def __str__(self):
        return self.value + " " + self.key
